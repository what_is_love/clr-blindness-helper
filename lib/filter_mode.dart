import 'dart:ui';

/// Main Abstract class for different filters.
/// To change the mode of filter in runtime you have to work with
/// FilterManager.
abstract class FilterMode {
  /// Function that converts provided color to the new by using filter for
  /// color blindness.
  Color filterColor(Color color);

  double _getDifferenceFromBasicColorValue(int initialValue) =>
      initialValue / 255;
}

/// This is standard filter (no changes with colors).
class NoFilter extends FilterMode {
  @override
  Color filterColor(Color color) {
    return color;
  }
}

/// This filter helps to avoid color combinations that could be difficult
/// for people with color blindness to differ (red-green-brown).
class RedFilter extends FilterMode {
  @override
  Color filterColor(Color color) {
    // if color is mostly red
    if (color.red > 180 && color.blue < 180 && color.green < 180) {
      // we make this colors be grey
      return Color.fromARGB(
          color.alpha,
          100,
          (230 * _getDifferenceFromBasicColorValue(color.green)).round(),
          (230 * _getDifferenceFromBasicColorValue(color.blue)).round());
    }
    // if color is mostly green
    else if (color.green > 180 && color.blue < 180 && color.red < 180) {
      return Color.fromARGB(
          color.alpha,
          50,
          (100 * _getDifferenceFromBasicColorValue(color.green)).round(),
          (230 * _getDifferenceFromBasicColorValue(color.blue)).round());
    }
    // if color is mostly yellow
    else if (color.red > 220 && color.green > 220 && color.blue < 180) {
      return Color.fromARGB(
          color.alpha,
          100,
          100 * _getDifferenceFromBasicColorValue(color.green).round(),
          50 * _getDifferenceFromBasicColorValue(color.blue).round());
    }
    // if the color is mostly brown
    else if (color.red > 120 && color.green > 120 && color.blue < 120) {
      return Color.fromARGB(
          color.alpha,
          25,
          (50 * _getDifferenceFromBasicColorValue(color.green)).round(),
          (50 * _getDifferenceFromBasicColorValue(color.blue)).round());
    }

    return color;
  }
}

/// This filter helps to filter screen for people with problems
/// with green color.
///
/// Filter changes color to avoid "dangerous" combinations of colors.
class GreenFilter extends FilterMode {
  @override
  Color filterColor(Color color) {
    // if color is mostly red
    if (color.red > 180 && color.blue < 180 && color.green < 180) {
      // we make this colors be grey
      return Color.fromARGB(
          color.alpha,
          (230 * _getDifferenceFromBasicColorValue(color.green)).round(),
          50,
          (230 * _getDifferenceFromBasicColorValue(color.blue)).round());
    }
    // if color is mostly green
    else if (color.green > 180 && color.blue < 180 && color.red < 180) {
      return Color.fromARGB(
          color.alpha,
          (230 * _getDifferenceFromBasicColorValue(color.green)).round(),
          50,
          (50 * _getDifferenceFromBasicColorValue(color.blue)).round());
    }
    // if color is mostly yellow
    else if (color.red > 220 && color.green > 220 && color.blue < 180) {
      return Color.fromARGB(
          color.alpha,
          230 * _getDifferenceFromBasicColorValue(color.green).round(),
          100,
          230 * _getDifferenceFromBasicColorValue(color.blue).round());
    }
    // if the color is mostly brown
    else if (color.red > 120 && color.green > 120 && color.blue < 120) {
      return Color.fromARGB(
          color.alpha,
          (100 * _getDifferenceFromBasicColorValue(color.green)).round(),
          25,
          (50 * _getDifferenceFromBasicColorValue(color.blue)).round());
    }

    return color;
  }
}
