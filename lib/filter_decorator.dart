import 'dart:ui';
import 'filter_manager.dart';

class FilterDecorator extends Color {
  FilterDecorator(Color color, FilterManager filterManager)
      : super(filterManager.filterMode.filterColor(color).value);
}
