import 'dart:ui';

import 'filter_mode.dart';

class FilterManager {
  static final FilterManager _instance = FilterManager._getInstance();
  FilterMode filterMode = NoFilter();

  FilterManager._getInstance();

  factory FilterManager () {
    return _instance;
  }
}
