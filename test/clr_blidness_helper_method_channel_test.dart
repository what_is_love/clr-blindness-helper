import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:clr_blidness_helper/clr_blidness_helper_method_channel.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MethodChannelClrBlidnessHelper platform = MethodChannelClrBlidnessHelper();
  const MethodChannel channel = MethodChannel('clr_blidness_helper');

  setUp(() {
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(
      channel,
      (MethodCall methodCall) async {
        return '42';
      },
    );
  });

  tearDown(() {
    TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger.setMockMethodCallHandler(channel, null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
