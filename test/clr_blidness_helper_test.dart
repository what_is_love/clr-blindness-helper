import 'package:flutter_test/flutter_test.dart';
import 'package:clr_blidness_helper/clr_blidness_helper.dart';
import 'package:clr_blidness_helper/clr_blidness_helper_platform_interface.dart';
import 'package:clr_blidness_helper/clr_blidness_helper_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockClrBlidnessHelperPlatform
    with MockPlatformInterfaceMixin
    implements ClrBlidnessHelperPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final ClrBlidnessHelperPlatform initialPlatform = ClrBlidnessHelperPlatform.instance;

  test('$MethodChannelClrBlidnessHelper is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelClrBlidnessHelper>());
  });

  test('getPlatformVersion', () async {
    ClrBlidnessHelper clrBlidnessHelperPlugin = ClrBlidnessHelper();
    MockClrBlidnessHelperPlatform fakePlatform = MockClrBlidnessHelperPlatform();
    ClrBlidnessHelperPlatform.instance = fakePlatform;

    expect(await clrBlidnessHelperPlugin.getPlatformVersion(), '42');
  });
}
