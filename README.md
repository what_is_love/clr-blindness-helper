# clr_blindness_helper

This is the Color Blindness Helper. The main purpose of this package is to help to adapt
colors on the screen for the people with color blindness.

## Demo

![Alt Text](https://i.imgur.com/4xe1Afx.gif)

## Getting Started

To start working with package you should import 3 main parts:

    - filter_decorator

    - filter_manager

    - filter_mode


## How to use the package

To initialize all filters you should create FilterManager() at any part of project 
(see example for detailed code). The FilterManager is the Singleton, so you can call constructor
anywhere and will get the same instance.

To show the package, where are the colors to adapt, you have to use FilterDecorator (see example).
FilterDecorator connects to FilterManager and converts your colors when it needed.

To switch filters you have to change mode of FilterManage (see example).
filterManage.filterMode = 'Here you put the instance of filter'.

Currently available filters:

    - NoFilter() - disable filter

    - RedFilter() - filter for people with protanopia (cannot see red).

    - GreenFilter() - filter for people with deuteranopia (cannot see green).

# Simple code


    class _MyHomePageState extends State<MyHomePage> {
    // Here we init the filter manager.
    FilterManager filterManager = FilterManager();
    // change filter mode
    filterManager.filtermode = RedFilter();

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: const Text('Test Color Blindness'),
                // here we connect color to decorator
                backgroundColor: FilterDecorator(Colors.amber, filterManager)),
            body: SomeWidget()



## How it works?

Filters help to avoid "dangerous" combinations of colors. For example for people with protanopia 
such combinations are red&green, red&brown.
To help people with color blindness see the difference between colors, filters make the difference 
on the colors that are visible. Example: if person does not see red color, then filter will convert
it to the visible spectre (blue/green).
