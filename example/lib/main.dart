/// Firstly, you have to import 3 main parts

import 'package:clr_blidness_helper/filter_decorator.dart';
import 'package:clr_blidness_helper/filter_manager.dart';
import 'package:clr_blidness_helper/filter_mode.dart';

import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  /// Here we init the filter manager.
  FilterManager filterManager = FilterManager();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Test Color Blindness')),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: Text("No filter"),
                  onPressed: () {
                    /// This is one of the examples, how to update filters
                    /// and colors on the screen.
                    /// When you change the filterMode, you have to rebuild your
                    /// screen.
                    setState(() {
                      filterManager.filterMode = NoFilter();
                    });
                  },
                ),
                ElevatedButton(
                  child: Text("Red filter"),
                  onPressed: () {
                    setState(() {
                      filterManager.filterMode = RedFilter();
                    });
                  },
                ),
                ElevatedButton(
                  child: Text("Green filter"),
                  onPressed: () {
                    setState(() {
                      filterManager.filterMode = GreenFilter();
                    });
                  },
                ),
              ],
            ),
            /// This is the pie chart to show how filters work.
            Container(
                height: 200,
                padding: EdgeInsets.only(top: 50),
                child: PieChart(PieChartData(
                    centerSpaceRadius: 0,
                    sectionsSpace: 0,
                    borderData: FlBorderData(show: false),
                    sections: [
                      PieChartSectionData(
                          value: 10,
                          color:
                          /// To init the color for auto updating after changing
                          /// the mode you should decorate it with
                          /// FilterDecorator(). Also you have to provide the
                          /// filterManager to connect to.
                          FilterDecorator(Colors.lightGreen, filterManager),
                          radius: 110),
                      PieChartSectionData(
                          value: 10,
                          color: FilterDecorator(Colors.amber, filterManager),
                          radius: 110),
                      PieChartSectionData(
                          value: 10,
                          color: FilterDecorator(Colors.yellow, filterManager),
                          radius: 110),
                      PieChartSectionData(
                          value: 10,
                          color: FilterDecorator(Colors.orange, filterManager),
                          radius: 110),
                      PieChartSectionData(
                          value: 30,
                          color: FilterDecorator(Colors.red, filterManager),
                          radius: 110)
                    ]))),
          ],
        ));
  }
}
